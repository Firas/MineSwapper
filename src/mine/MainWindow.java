package mine;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

public class MainWindow {
	private JFrame frame;
	private JPanel panelParameters, panel;
	private JLabel lblRows, lblColumns, lblMines;
	private JSpinner spinnerRows, spinnerColumns, spinnerMines;
	private JButton buttonNew, buttonNext;
	public int rows, columns, mines;
	public MineBox boxes[][];
	
	public static int deduction = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		rows = 10;
		columns = 10;
		mines = 10;
		
		GridBagLayout layoutMain = new GridBagLayout(), layoutParameters = new GridBagLayout();
		GridBagConstraints constraintMain = new GridBagConstraints(), constraintParameters = new GridBagConstraints();
		constraintMain.gridwidth = GridBagConstraints.REMAINDER;
		constraintMain.anchor = GridBagConstraints.NORTHWEST;
		constraintMain.fill = GridBagConstraints.HORIZONTAL;
		constraintMain.weightx = 1.0;
		constraintParameters.anchor = GridBagConstraints.NORTHWEST;
		
		frame = new JFrame("Mine Swapper");
		frame.setBounds(100, 100, 400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(layoutMain);
		
		panelParameters = new JPanel();
		layoutMain.setConstraints(panelParameters, constraintMain);
		frame.getContentPane().add(panelParameters);
		panelParameters.setLayout(layoutParameters);
		panelParameters.setBounds(0, 0, frame.getWidth(), 20);
		
		int insetTop = 3, insetBottom = 5;
		lblRows = new JLabel("Rows: ");
		constraintParameters.insets = new Insets(insetTop, 3, insetBottom, 0);
		layoutParameters.setConstraints(lblRows, constraintParameters);
		panelParameters.add(lblRows);
		
		spinnerRows = new JSpinner(new SpinnerNumberModel(rows, 10, 20, 1));
		spinnerRows.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e){
				rows = (int)(spinnerRows.getValue());
				initializeMap();
			}
		});
		constraintParameters.insets = new Insets(insetTop, 0, insetBottom, 5);
		layoutParameters.setConstraints(spinnerRows, constraintParameters);
		panelParameters.add(spinnerRows);
		
		lblColumns = new JLabel("Columns: ");
		constraintParameters.insets = new Insets(insetTop, 3, insetBottom, 0);
		layoutParameters.setConstraints(lblColumns, constraintParameters);
		panelParameters.add(lblColumns);
		
		spinnerColumns = new JSpinner(new SpinnerNumberModel(columns, 10, 40, 1));
		spinnerColumns.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e){
				columns = (int)(spinnerColumns.getValue());
				initializeMap();
			}
		});
		constraintParameters.insets = new Insets(insetTop, 0, insetBottom, 5);
		layoutParameters.setConstraints(spinnerColumns, constraintParameters);
		panelParameters.add(spinnerColumns);
		
		lblMines = new JLabel("Mines: 0 / ");
		constraintParameters.insets = new Insets(insetTop, 3, insetBottom, 0);
		layoutParameters.setConstraints(lblMines, constraintParameters);
		panelParameters.add(lblMines);
		
		spinnerMines = new JSpinner(new SpinnerNumberModel(mines, 10, rows * columns / 4, 1));
		spinnerMines.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e){
				mines = (int)(spinnerMines.getValue());
				initializeMap();
			}
		});
		constraintParameters.insets = new Insets(insetTop, 0, insetBottom, 5);
		layoutParameters.setConstraints(spinnerMines, constraintParameters);
		panelParameters.add(spinnerMines);
		
		buttonNew = new JButton("New");
		constraintParameters.insets = new Insets(insetTop, 3, insetBottom, 0);
		layoutParameters.setConstraints(buttonNew, constraintParameters);
		panelParameters.add(buttonNew);
		buttonNew.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				initializeMap();
			}
		});
		
		buttonNext = new JButton("Next");
		constraintParameters.insets = new Insets(insetTop, 3, insetBottom, 0);
		layoutParameters.setConstraints(buttonNext, constraintParameters);
		panelParameters.add(buttonNext);
		buttonNext.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				deduction++;
			}
		});
		
		panel = new JPanel();
		constraintMain.anchor = GridBagConstraints.SOUTH;
		constraintMain.fill = GridBagConstraints.BOTH;
		constraintMain.weighty = 1.0;
		layoutMain.setConstraints(panel, constraintMain);
		frame.getContentPane().add(panel);
		
		initializeMap();
	}

	public void initializeMap(){
		int i, j;
		frame.setMinimumSize(new Dimension(32 * columns, 32 * (1 + rows)));
		panel.removeAll();
		panel.setLayout(new GridLayout(rows, columns));
		boxes = new MineBox[rows][];
		MineBox.setBoxes(MainWindow.this);
		
		deduction = 0;
		
		for(i = 0; i < rows; i++){
			boxes[i] = new MineBox[columns];
			for(j = 0; j < columns; j++){
				panel.add(boxes[i][j] = new MineBox(i, j));
			}
		}
		
		if(mines > rows * columns / 4) mines = rows * columns / 4;
		spinnerMines.setModel(new SpinnerNumberModel(mines, 10, rows * columns / 4, 1));
		
		frame.revalidate(); frame.repaint();
		lblMines.setText("Mines: 0 / ");
	}
	
	public void setFlags(int f){
		lblMines.setText("Mines: " + f + " / ");
	}
	
}
