package mine;

public class OpenNeighbor extends Thread {
	public MineBox mb;
	public int count;
	public OpenNeighbor(MineBox mb, int count){
		this.mb = mb;
		this.count = count;
		System.out.println("watch " + mb.getRow() + ", " + mb.getColumn());
	}
	public void run(){
		try{
			while(MineBox.start && mb.getNeighbors() - mb.getMines() > mb.getOpens()){
				if(count < MainWindow.deduction){
					if(mb.openNeighborTip()){
						System.out.println("open " + mb.getRow() + ", " + mb.getColumn());
						return;
					}
				}
				sleep(100);
			}
			while(count + 1 > MainWindow.deduction){
				if(!MineBox.start) return;
				sleep(100);
			}
			mb.flagNeighbors();
			System.out.println("flag " + mb.getRow() + ", " + mb.getColumn());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
